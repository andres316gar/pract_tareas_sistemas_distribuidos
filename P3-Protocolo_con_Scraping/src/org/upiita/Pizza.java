package org.upiita;

import org.openqa.selenium.WebElement;

public class Pizza {
    String brand;
    String type;
    String description;
    String price;

    public Pizza(String brand, WebElement wElement){
        this.brand = brand;
        String text = wElement.getText();
        String[] lines = text.split("\\n");
        int count = 0;
        for (String line: lines) {
            switch (count){
                case 0:
                    this.type = line;
                    break;
                case 1:
                    this.description = line;
                    break;
                case 2:
                    this.price = line;
            }
            count++;
        }
    }
}
